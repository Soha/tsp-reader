#
# This file reads a TSPLib instance and return a (symmetric) matrix of the
# distances between nodes.
#

from collections import OrderedDict
import numpy as np


def symmetrize(a):
    """
    Symmetrize a matrix.
    """
    return a + a.T - np.diag(a.diagonal())


def get_distance_2d(x1, y1, x2, y2, ceiled):
    """
    Euclidean distance.
    """
    dist = np.sqrt((x1 - x2)**2 + (y1 - y2)**2)

    if ceiled:
        return np.ceil(dist)
    else:
        return dist


def get_distance_att(xi, xj, yi, yj):
    """
    Pseudo-Euclidean 'ATT' distance.
    """
    xd = xi - xj
    yd = yi - yj
    rij = np.sqrt((xd * xd + yd * yd) / 10.0)
    tij = round(rij)

    if tij < rij:
        dij = tij + 1
    else:
        dij = tij

    return dij


def get_distance_geo(x1, y1, x2, y2):
    """
    Geodesic distance.
    """
    latitude_x1 = get_rad_int(x1)
    latitude_x2 = get_rad_int(x2)

    longtitude_y1 = get_rad_int(y1)
    longtitude_y2 = get_rad_int(y2)

    RRR = 6378.388
    q1 = np.cos(longtitude_y1 - longtitude_y2)
    q2 = np.cos(latitude_x1 - latitude_x2)
    q3 = np.cos(latitude_x1 + latitude_x2)

    return int(RRR * np.arccos(.5 * ((1. + q1) * q2 -
                                     (1. - q1) * q3)) + 1.) * 1.0


def get_rad_int(x):
    """
    Get the distance from geographic coordinates.

    TODO: verify that using `np.pi` instead of $3.141592$ does not matter
    """
    minute, degree = np.modf(x)

    return np.pi * (degree + 5.0 * minute / 3.0) / 180.0


def get_edge_weights_EUC_2D(edges, n_nodes, ceiled):

    coordinate = OrderedDict()

    for line in edges:
        data = line.split()
        i, x, y = int(data[0]) - 1, float(data[1]), float(data[2])
        coordinate[i] = x, y

    edgeWeightMatrix = np.zeros((n_nodes, n_nodes))

    for i in range(n_nodes):
        for j in range(i, n_nodes):
            x1, y1 = coordinate[i]
            x2, y2 = coordinate[j]

            eu_distance = get_distance_2d(x1, y1, x2, y2, ceiled)

            edgeWeightMatrix[i][j] = eu_distance
            edgeWeightMatrix[j][i] = eu_distance

    return edgeWeightMatrix


def get_edge_weights_ATT(data, n_nodes):
    """
    Weird euclidian distance, data contains coordinates for each node.
    """
    coordinates = OrderedDict()

    for line in data:
        i, x, y = map(int, line.split(" "))
        coordinates[i - 1] = (x, y)

    matrix = np.zeros((n_nodes, n_nodes))

    for i in range(n_nodes):
        for j in range(i, n_nodes):
            xi, yi = coordinates[i]
            xj, yj = coordinates[j]
            dist = get_distance_att(xi, xj, yi, yj)

            matrix[i][j] = dist
            matrix[j][i] = dist

    return matrix


def get_edge_weights_GEO(edges, n_nodes):
    """
    Geodesic coordinate system.
    """
    geo_coordinate = OrderedDict()

    for line in edges:
        data = line.split()
        i, x, y = int(data[0]) - 1, float(data[1]), float(data[2])
        geo_coordinate[i] = x, y

    edgeWeightMatrix = np.zeros((n_nodes, n_nodes))

    for i in range(n_nodes):
        for j in range(i + 1, n_nodes):
            x1, y1 = geo_coordinate[i]
            x2, y2 = geo_coordinate[j]
            dist = get_distance_geo(x1, y1, x2, y2)

            edgeWeightMatrix[i][j] = dist
            edgeWeightMatrix[j][i] = dist

    return edgeWeightMatrix


def get_edge_weights_LOWER_DIAG_ROW(edges, n_nodes):
    """
    Lower diagonal matrix; all elements in reverse order with zeroes.
    """
    matrix = np.zeros((n_nodes, n_nodes))
    row = []
    i = 0

    for line in edges:
        data = [float(x) for x in line.split()]

        for v in data:
            if v == 0:
                # Trick to extend the list with zeroes
                base = [0] * (n_nodes - len(row))

                matrix[i] = row + base
                row = []
                i += 1
            else:
                row.append(v)

    return symmetrize(matrix)


def get_edge_weights_UPPER_DIAG_ROW(edges, n_nodes):
    """
    Upper triangular matrix with diagonal elements; so a long list of numbers
    that should add up to `n_nodes` to be added, then pad with zeroes and start
    again.
    """
    edgeWeightMatrix = np.zeros((n_nodes, n_nodes))
    row = []
    i = 0

    for line in edges:
        data = [float(x) for x in line.split()]

        if len(data) + len(row) >= n_nodes:
            need = n_nodes - len(row)
            row.extend(data[:need])
            data = data[need:]

            edgeWeightMatrix[i] = row

            # Upper triangular matrix
            row = [0] * i
            i += 1

        row.extend(data)

    return symmetrize(edgeWeightMatrix)


def get_edge_weights_UPPER_ROW(edges, n_nodes):
    """
    Upper row format; complete matrix rows given at once, without zeroes.
    """
    edgeWeightMatrix = np.zeros((n_nodes, n_nodes))
    row = [0]
    i = 0

    for line in edges:
        data = [float(x) for x in line.split()]

        # Obviously not in the same format once it gets bigger...  Yes! This is
        # a while loop as, sometimes, you can have perfectly aligned rows. The
        # last check is here to prevent errors on the last entry.
        while len(row) + len(data) >= n_nodes and i < n_nodes:
            need = n_nodes - len(row)
            row.extend(data[:need])
            edgeWeightMatrix[i] = row

            i += 1
            row = [0] * (i + 1)
            data = data[need:]

        row.extend(data)

    return symmetrize(edgeWeightMatrix)


def get_edge_weights_FULL_MATRIX(edges, n_nodes):
    """
    Full matrix; all entries given.
    """
    edgeWeightMatrix = np.zeros((n_nodes, n_nodes))

    for i, line in enumerate(edges):
        edgeWeightMatrix[i] = np.fromstring(line, sep=' ')

    return edgeWeightMatrix


def clean_up(filename):
    """
    Open up a file and clean the data.
    """
    tspInstance = OrderedDict()
    edges = []

    with open(filename, 'r') as f:
        for line in f:
            if ":" in line:
                key, value = line.split(":")
                clean = value.strip()

                if clean.isdigit():
                    val = int(clean)
                else:
                    val = clean

                tspInstance[key] = val
            elif "EOF" in line or "DISPLAY_DATA_SECTION" in line:
                # Have to put EOF here to avoid empty lines at the end of
                # files...
                break
            elif ("NODE_COORD_SECTION" not in line
                  and "EDGE_WEIGHT_SECTION" not in line):
                edges.append(line.strip())

    return tspInstance, edges


def readTSPfile(filename):
    """
    Read a file, that is select the proper function to read the data.
    """
    tspInstance, edges = clean_up(filename)

    weight_type = tspInstance['EDGE_WEIGHT_TYPE']
    n_nodes = tspInstance['DIMENSION']

    if weight_type == "ATT":
        tspInstance["EDGES"] = get_edge_weights_ATT(edges, n_nodes)
    elif weight_type == 'EUC_2D' or weight_type == "CEIL_2D":
        tspInstance['EDGES'] = get_edge_weights_EUC_2D(edges, n_nodes,
                                                       "CEIL" in weight_type)
    elif weight_type == 'GEO':
        tspInstance['EDGES'] = get_edge_weights_GEO(edges, n_nodes)
    elif weight_type == 'EXPLICIT':
        weight_format = tspInstance['EDGE_WEIGHT_FORMAT']

        if weight_format == 'LOWER_DIAG_ROW':
            tspInstance['EDGES'] = get_edge_weights_LOWER_DIAG_ROW(
                edges, n_nodes=tspInstance['DIMENSION'])
        elif weight_format == 'UPPER_ROW':
            tspInstance['EDGES'] = get_edge_weights_UPPER_ROW(
                edges, n_nodes=tspInstance['DIMENSION'])
        elif weight_format == 'UPPER_DIAG_ROW':
            tspInstance['EDGES'] = get_edge_weights_UPPER_DIAG_ROW(
                edges, n_nodes)
        elif weight_format == 'FULL_MATRIX':
            tspInstance['EDGES'] = get_edge_weights_FULL_MATRIX(edges, n_nodes)
        else:
            raise ValueError(
                "FORMAT '{}' not supported!".format(weight_format))
    else:
        raise TypeError("TYPE '{}' not supported!".format(weight_type))

    return tspInstance
