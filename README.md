# TSPLib Reader

A reader for the
[TSPLib](https://www.iwr.uni-heidelberg.de/groups/comopt/software/TSPLIB95/)
written in Python.

# Features

The features you would expect from a TSPLib reader, although I did not find one
doing it:

 * Read all format of the TSPLib.
 * Translate the information for each instance into a distance matrix.
 * Tested!

I found a few readers lying around, but they would simply parse the files
without applying the distance functions.

## Requirements

This code needs [numpy](http://www.numpy.org/) to run.

## Testing

I used the XML variant of the TSP instances which give distance matrices -- but
are unwieldingly large.

I provided the testing data in the `test` branch so it is "properly" organised
-- as in, how I use it.

To run the tests you will need additional libraries:

 * [untangle](https://github.com/stchris/untangle) to read XML files.

# TODO

Turn this into an actual package.
